import datetime
import json
import pprint


#Function Description:Function to increment the days yyyymmdd
def incrementdate(Days):
    date = datetime.datetime.now()
    date+= datetime.timedelta(days=Days)
    return (date.strftime('%Y-%m-%d'))

#Function Description:Increments dates in the JSON file
def update_dates_in_json_file(filename):
  with open(filename, 'r') as f:
    data = json.load(f)
  data['reservations'][0]['startDate'] = incrementdate(2)
  data['reservations'][0]['endDate'] = incrementdate(3)
  with open(filename, 'w') as jsonFile:
    json.dump(data, jsonFile, indent=2)
  return 1


